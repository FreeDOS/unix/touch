# TOUCH

Set the date + time stamp on a file like *nix "touch"

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## TOUCH.LSM

<table>
<tr><td>title</td><td>TOUCH</td></tr>
<tr><td>version</td><td>1.4.4a</td></tr>
<tr><td>updated</td><td>2010-03-31</td></tr>
<tr><td>entered&nbsp;date</td><td>2012-09-22</td></tr>
<tr><td>description</td><td>Set the date + time stamp on a file</td></tr>
<tr><td>keywords</td><td>touch, time stamp, unix</td></tr>
<tr><td>author</td><td>k -at- heidenstrom.gen.nz</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/util/file</td></tr>
<tr><td>platforms</td><td>DOS (2.0, 8086), FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>summary</td><td>Set the date + time stamp on a file like *nix "touch"</td></tr>
</table>
