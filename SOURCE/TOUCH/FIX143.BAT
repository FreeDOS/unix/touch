@echo off
if not exist %0 %0.bat
if not exist touch.com goto end
if not exist touch2.com copy /b touch.com touch2.com
attrib -r touch2.com
debug touch2.com < %0
goto end

; patch for Kris Heidenstrom's TOUCH.COM 1.4.3 (for FreeDOS)
; Monday 01 March 2010, rugxulo _AT_ gmail _DOT_ com

; what it does: lets -C also permit touching readonly +R files
; int 21h,3D '02'h (read/write) fails to open on +R files

; TODO: convert whole program from TASM to FASM; fix this; add LFN support

;NewCall=09C0
;NewEOF=0145h
;Patch=0146h
;CFlag=058Eh
;ret=0151h

; (only if you want to bump the version number to 1.4.'4' for -F)
;e 127
;34

a 09C0
; former home of "mov ax,3D02", patched with call to fix
call 0146

a 0145
; = 13 bytes below
; (1Ah = EOF / Ctrl-Z marker for "type")
db 1A
mov ax,3D02
test byte ptr [058E],1
jz 0151
; salc
db D6
ret

w
Q
Q

:end
for %%a in (touch touch2) do if not exist %%a.com goto bye
dir touch*.com | find /i "com"
echo on
REM ... generate an error (to prove it didn't work before)
attrib +r touch.com
touch2 touch.com
REM ... try again with -C (to prove it works now)
touch2 -C touch.com
@echo off
dir touch*.com | find /i "com"
:bye
